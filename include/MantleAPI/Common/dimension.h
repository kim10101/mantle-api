/*******************************************************************************
 * Copyright (c) 2021, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

//-----------------------------------------------------------------------------
/** @file  dimension.h */
//-----------------------------------------------------------------------------

#ifndef MANTLEAPI_COMMON_DIMENSION_H
#define MANTLEAPI_COMMON_DIMENSION_H

#include <MantleAPI/Common/floating_point_helper.h>
#include <units.h>

namespace mantle_api
{

struct Dimension3
{
  units::length::meter_t length{0};
  units::length::meter_t width{0};
  units::length::meter_t height{0};
};

constexpr bool operator==(const Dimension3& lhs, const Dimension3& rhs) noexcept
{
  return AlmostEqual(lhs.length, rhs.length) && AlmostEqual(lhs.width, rhs.width) && AlmostEqual(lhs.height, rhs.height);
}

constexpr bool operator!=(const Dimension3& lhs, const Dimension3& rhs) noexcept
{
  return !(lhs == rhs);
}

}  // namespace mantle_api

#endif  // MANTLEAPI_COMMON_DIMENSION_H
