/*******************************************************************************
 * Copyright (c) 2022, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

//-----------------------------------------------------------------------------
/** @file  route_definition.h */
//-----------------------------------------------------------------------------

#ifndef MANTLEAPI_MAP_LANE_DEFINITION_H
#define MANTLEAPI_MAP_LANE_DEFINITION_H

#include <cstdint>

namespace mantle_api
{

/// A LaneId does not necessarily have to be unique.
/// Depending on the scenario description it can repeat,
/// as e.g. for every uniqulely identifiable road (i.e. OpenDRIVE).
using LaneId = int64_t;

}  // namespace mantle_api

#endif  // MANTLEAPI_MAP_LANE_DEFINITION_H
